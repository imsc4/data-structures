#include"Strings.h"

Strings::Strings(){
	 LB=0;
	 UB=0;
	 S=new char[1];
	 S[0]='\0';
}


Strings::Strings(int u){
	  LB=0;
      UB=u;
      int i;
      cout<<"Enter the characters to the string one by one : ";
      S=new char[UB+1];
      for(int i=LB;i<UB;i++){
      	 cin>>S[i];
	  }
	  S[UB]='\0';
	}
	


int Strings::length(){
	int i=-1;
	while(*(S+(++i))!='\0');
	return i;
}


int Strings::compare(const Strings& s2){
	int i=0;
	while (S[i]==s2.S[i] && S[i]!='\0' && s2.S[i]!='\0'){
		i++;	
	}
	return S[i]-s2.S[i];
}	
	
char* Strings::concat(const Strings& s2){
	int l1=length();
	int l2=length();
	char* S3=new char [l1+l2+1];
	int i;
	for(i=0;i<l1-1;i++){
		S3[i]=S[i];
	}
	for(i=0;i<l2-1;i++){
		S3[l1+i]=s2.S[i];
	}
	return S3;
}
