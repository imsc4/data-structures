#include"Array1.cpp"
int main(){
	int s;
	int p;
	Array<int> myArray;
	myArray.insert_at_end(2);
	myArray.insert_at_end(5);
	myArray.insert_at_end(6);
	cout<<"INSERTION :";
	cout<<"\n1.at end";
	cout<<myArray;
	
	myArray.insert_at_beginning(1);
	myArray.insert_at_beginning(7);
	myArray.insert_at_beginning(14);
	cout<<"\n2.at beginning";
	cout<<myArray;
	
	myArray.insert_at_index(8,3);
	myArray.insert_at_index(10,5);
	myArray.insert_at_index(9,3);
	cout<<"\n3.at index";
	cout<<myArray<<endl;
	cout<<" ";
	
	cout<<"DELETION : ";
	cout<<"\n1.at end";
	myArray.delete_at_end(5);
	cout<<myArray;
	
	cout<<"\n2.at beginnning";
	myArray.delete_at_beginning(7);
	cout<<myArray;
	
	cout<<"\n3.at index";
	myArray.delete_at_index(6);
	cout<<myArray;
	
	cout<<"Current Array elements :\n";
	myArray.list_distinct();
	cout<<"Frequency list\n";
	myArray.frequency();
	
	
	cout<<"SORTING : ";
	cout<<myArray;

	cout<<"Bubble sort ";
	myArray.bubble_sort();
	cout<<myArray;	
		
	cout<<"insertion sort ";
	myArray.insertion_sort();
	cout<<myArray;
	
	cout<<"Quick sort ";
	myArray.quick_sort(1,3);
	cout<<myArray;
	
	cout<<"Merge Sort";
	myArray.merge_sort(1,3);
	cout<<myArray;
	

		
	cout<<"enter a element to search :";
	cin>>s;
	cout<<"linear search \n";
	cout<<"element found in : "<<myArray.linear_search(s)<<endl;

	cout<<"binary search  ";
	cout<<"element found in : "<<myArray.binary_search(s)<<endl;
	
	cout<<"enter the position to rotate :";
	cin>>p;
	
	myArray.rotate_clockwise(p);
	cout<<myArray;
	myArray.rotate_anticlockwise(p);
	cout<<myArray;
	


		

/*	Array<float> myfArray;
	myfArray.insert_at_end(3.5);
	myfArray.insert_at_beginning(14.3);
	myfArray.insert_at_index(2.3,8);
	cout<<myArray;
	
	myfArray.delete_at_beginning(4);
	myfArray.delete_at_end(2);
	myfArray.delete_at_index(6);
	cout<<myfArray;
	
	myfArray.delete_at_end(5);
	myfArray.delete_at_beginning(2);
	myfArray.delete_at_index(6);
	cout<<myfArray;
	
	cout<<myfArray.binary_search(3.5);
	cout<<myfArray.linear_search(2.3);
	cout<<myfArray;
	myfArray.selection_sort();
	cout<<myfArray;	*/

    return 0;
}
